///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

require [ 
    "dojo/_base/declare", 
    "dojo/dom",
    "dojo/json",
    "dojo/parser", 
    "dojo/_base/event",
    "dojo/_base/lang", 
    "dojo/on", 
    "dijit/registry",
    "dojox/validate/_base",
    "dojo/date/stamp",
    "dojo/store/Memory",
    "dojo/ready",
    "dojox/lang/functional/object",    
    "app/base.js",
    "app/server.js",    
    "dojo/domReady!" ], (
        declare,
        dom,
        json, 
        parser, 
        bevent, 
        lang, 
        _on, 
        registry, 
        validation, 
        stamp, 
        memory, 
        ready,
        func, 
        base,
        server) ->



    ready(->





        declare "SignUpPage", [],




            constructor:()->
                this.checker = new server("/xhr/signupcheck")




            postCreate:()->
                Lang.set("options", window.base.locales.forselect)
                Lang.set("value", "en-us")
                Uname.validator = lang.hitch(this, "check_uname")
                Uname.check_result = this.check_result
                Email.validator = lang.hitch(this, "check_email")
                Email.check_result = this.check_result
                Email.check_2nd = this.check_2nd
                Email2.validator = lang.hitch(Email, "check_2nd")
                Password.validator = this.check_password
                Password.check_2nd = this.check_2nd
                Password2.validator = lang.hitch(Password, "check_2nd")
                Submit.onClick = lang.hitch(this, "submit")




            check_uname: (v) ->
                if not validation.isText(v, {minlength: 4, maxlength: 12})
                    Uname.set('invalidMessage', 
                        window.base.locales.get("UnameRules"))
                    return(false)
                if not /^[a-zA-Z][a-zA-z0-9]*$/.test(v)
                    Uname.set('invalidMessage', 
                        window.base.locales.get("UnameRules"))
                    return(false)

                if not Uname.focused and Uname.lastcheckvalue != v
                    Uname.lastcheckvalue = v
                    data = new stack()
                    data.append(window.base.source, {"uname": v})
                    this.checker.on("loaded", lang.hitch(Uname, "check_result"))
                    this.checker.on("error", lang.hitch(Uname, "check_result"))
                    this.checker.run(data)
                else
                    return(Uname.checkstate)
                return(false)




            check_email: (v) ->
                Email2.set("value", "")
                if not validation.isEmailAddress(v)
                    Email.set('invalidMessage', 
                        window.base.locales.get("InvalidEmail"))
                    return(false)

                if not Email.focused and Email.lastcheckvalue != v
                    Email.lastcheckvalue = v
                    data = new stack()
                    data.append(window.base.source, {"email": v})
                    this.checker.on("loaded", lang.hitch(Email, "check_result"))
                    this.checker.on("error", lang.hitch(Email, "check_result"))
                    this.checker.run(data)
                else
                    return(Email.checkstate)
                return(false)




            check_result: (result) ->
                if result[0]["data"]["result"] == false
                    this.checkstate = true
                    this.set("state","")
                else                    
                    this.checkstate = false
                    this.set("state","Error")
                    this.set('invalidMessage', 
                        window.base.locales.get("Registered"))




            check_2nd: (v) ->
                if this.value == v
                    return(true)
                else
                    return(false)




            check_password: (v) ->
                Password2.set("value", "")
                if Password.value and validation.isText(v, {minlength: 8, maxlength: 128})
                    return(true)
                this.set('invalidMessage', window.base.locales.get("InvalidPassword"))
                return(false)




            submit: (c) ->
                if Form.isValid()
                    data = new stack()
                    data.append(window.base.source, {
                        "lang" : Lang.value,
                        "uname": Uname.value,
                        "fname" : FirstName.value,
                        "lname" : LastName.value,
                        "email" : Email.value,
                        "passw" : Password.value,
                        "captc" : Captcha.value})
                    sender = new server("/xhr/signup")
                    sender.on("loaded", lang.hitch(this, "result"))
                    sender.on("error", lang.hitch(this, "error"))
                    sender.run(data)





            result: (respond) ->
                try
                    construct.destroy("pmessage")
                catch
                    null
                if respond[0]["data"].hasOwnProperty("fail")
                    window.base.server_err = respond[0]["data"]["fail"]
                    window.base.handle_messages(MsgArea.id, pos ="before", persistent ="true")
                if respond[0]["data"].hasOwnProperty("success") 
                    location.href = "/signupok"





            error: (e) ->
                console.log e
                window.base.server_err = "ServerError"
                window.base.handle_messages(MsgArea.id)






        base("signup")
        page = new SignUpPage()

        initialized = ->
            page.postCreate()


        window.base.load_locales(initialized)


        )
