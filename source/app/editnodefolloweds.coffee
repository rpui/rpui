///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

require [
    "dojo/ready",
    "dojo/request", 
    "dojo/_base/declare", 
    "dijit/registry",
    "app/base",
    "dojo/_base/lang",
    "dojo/dom-construct",
    "app/server", 
    "app/msg",    
    "app/docklist",
    "dojo/store/Memory",
    "dojo/dom",
    "dijit/form/Button",
    "dijit/form/CheckBox",
    "dijit/form/ValidationTextBox",
    "dojox/validate/web",
    "dojo/domReady!" ], (
        ready,
        request,
        declare,
        registry,
        base,
        lang,
        domConstruct,
        server,
        msg,        
        docklist,
        memory,
        dom,
        Button,
        CheckBox,
        ValidationTextBox,
        web) ->



    ready(->




        declare "EditNodeFolloweds", [],




            constructor:()->
                this.xhrtasks = new server("/xhr/gettasks")
                this.xhrtasks.on("loaded", lang.hitch(this, "tasks_loaded"))
                this.xhrtasks.on("error", lang.hitch(this, "tasks_error"))
                this.tasklist = []
                





            postCreate:()->
                if window.base.server_err != ""
                    window.base.handle_messages("NodeNameLabel")
                dom.byId("NodeCancelLink").href = "/u/editnode/"+window.base.server_db["name"]
                
                NodeName.set("value", window.base.server_db["name"])
                FollowedDelButton.onClick = lang.hitch(this, "followed_del")
                UserSearchButton.onClick = lang.hitch(this, "user_search")
                AddTasksButton.onClick = lang.hitch(this, "followed_add")
                for t in Object.values(window.base.server_db["tlist"])
                    NodeFollowed.domNode.appendChild( domConstruct.create("option", {
                        innerHTML:t}))
                this.update_tlist()




            followed_del: (c) ->
                for obj in NodeFollowed.getSelected()
                    obj.remove()
                this.update_tlist()




            followed_add: (c) ->
                for cb in this.tasklist
                    has = false
                    if cb.checked
                        for o in NodeFollowed.containerNode.options
                            if o.value == cb.value
                                has = true
                        if !has
                            NodeFollowed.domNode.appendChild( 
                                domConstruct.create("option", {innerHTML:cb.value}))
                this.update_tlist()


                    

            update_tlist: ()->
                v = []
                for o in NodeFollowed.containerNode.options
                    v.push(o.value)
                NodeTList.value = v




            user_search: () ->
                UserTasksPane.destroyDescendants()
                data = new stack()
                data.append(window.base.source, {"uname": UserName.value, "nname":NodeName.value})
                this.xhrtasks.run(data)




            tasks_loaded: (respond) ->
                this.tasklist = []
                if respond[0]["data"].hasOwnProperty("fail")
                    window.base.server_err = respond[0]["data"]["fail"]
                    window.base.handle_messages(UserName.id)
                else if respond[0]["data"].hasOwnProperty("tasks")
                    d = respond[0]["data"]
                    for nodename in Object.keys(d["nodes"])
                        n =nodename.charAt(0).toUpperCase()+nodename.slice(1)
                        UserTasksPane.domNode.appendChild(domConstruct.create("br"))
                        UserTasksPane.domNode.appendChild( 
                            domConstruct.create("label", {
                                innerHTML: n+" : "+d["nodes"][nodename]}))
                        UserTasksPane.domNode.appendChild(domConstruct.create("br"))
                        for uri in Object.keys(d["tasks"])
                            u = uri.split("/")[3]
                            u = u.charAt(0).toUpperCase()+u.slice(1)
                            if d["tasks"][uri]["node"] == nodename
                                checkbox = new CheckBox({value:uri})
                                UserTasksPane.addChild( checkbox )
                                UserTasksPane.domNode.appendChild( 
                                    domConstruct.create("label", {
                                        innerHTML:" "+u+" : "+d["tasks"][uri]["desc"]
                                        for:checkbox.id}))
                                UserTasksPane.domNode.appendChild(domConstruct.create("br"))
                            this.tasklist.push(checkbox)
                        



            tasks_error: (e) ->
                console.log(e)
                #window.base.server_err = 
                #window.base.handle_messages(this.followedsearch.id)





            
        base("editnodefolloweds")
        page = new EditNodeFolloweds()

        initialized = ->
            page.postCreate()


        window.base.load_locales(initialized)
        


        )
