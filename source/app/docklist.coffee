///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

define \
    ["dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/dom-construct",
    "dojo/Evented", 
    "dijit/layout/ContentPane",
    "dijit/form/Button",
    "dijit/_WidgetBase",
    "app/dockpane",
    "app/server",
    "app/msg",   
    "dojo/domReady!"],(
        declare, 
        lang,
        domConstruct,
        evented,
        ContentPane,
        Button,
        WidgetBase,
        dockpane,
        server,
        msg) ->





        declare "DockList", [ ContentPane, evented],
            



            constructor: (args) ->
                declare.safeMixin(this, args)
                this.style += "padding:0px;"
                this.dList = []
                this.sender = new server("/xhr/updatedock")
                this.sender.on("loaded", lang.hitch(this, "saved"))
                this.sender.on("error", lang.hitch(this, "notsaved"))


              

            postCreate: () ->
                if window.base.server_db.dock.hasOwnProperty("order")
                    docks = window.base.server_db.dock.order
                    for i in Object.keys(docks)
                        this.addChild(this._newPane(docks[i]))
                else
                    this.addChild(this._newPane())
                this.MsgArea = window.base.navlist.add_item(
                    "Save Changes", 
                    lang.hitch(this, "save"))




            _newPane: (dock={tid:"",title:"",editable:false,widget:"",constants:{},variables:{}})->
                pane = new DockPane(dock)
                pane.on("onMoveUp", lang.hitch(this, "_moveUp"))
                pane.on("onMoveDown", lang.hitch(this, "_moveDown"))
                pane.on("onAddUp", lang.hitch(this, "_addUp"))
                pane.on("onAddDown", lang.hitch(this, "_addDown"))
                pane.on("onClose", lang.hitch(this, "_close"))
                this.dList.push(pane)
                return(pane)




            _findOrder: (obj)->
                for dock, i in this.domNode.children
                    if dock.id == obj.id
                        return(i)




            _findObj: (id)->
                for obj in this.dList
                    if id == obj.id
                        return(obj)




            _moveUp: (obj) ->
                objPos = this._findOrder(obj)
                targetPos = objPos - 1
                if targetPos > -1
                    this.addChild(obj, targetPos)




            _moveDown: (obj) ->
                objPos = this._findOrder(obj)+1
                targetPos = objPos-1
                if targetPos > -1
                    obj = this._findObj(this.domNode.children[objPos].id)
                    this.addChild(obj, targetPos)




            _addUp: (obj) ->
                pos = this._findOrder(obj)
                this.addChild(this._newPane(), pos)




            _addDown: (obj) ->
                pos = this._findOrder(obj)+1
                this.addChild(this._newPane(), pos)




            _close: (obj) ->
                if this.dList.length > 1
                    for item, i in this.dList
                        if item.id == obj.id
                            this.dList.splice(i,1)
                            break
                    this.removeChild(obj)
                    obj.destroyRecursive()




            save: () ->
                docks = {}
                order = {}
                for item, i in this.dList
                    docks[item.id] = item.get_dock()
                for dock, i in this.domNode.children
                    order[i] = docks[dock.id]
                m = {nid:window.base.server_db.dock.nid, order:order}
                data = new stack()
                data.append(window.base.source, m)
                this.sender.run(data)




            saved: (respond) ->
                if respond[0]["data"].hasOwnProperty("fail")
                    window.base.server_err = respond[0]["data"]["fail"]
                    window.base.handle_messages(this.MsgArea)
                else
                    location.href = "/"+window.base.server_db["uname"]+"/"+window.base.server_db["node"]["name"]




            notsaved: (e) ->
                console.log e
                window.base.server_err = "ServerError"
                window.base.handle_messages(MsgArea)
