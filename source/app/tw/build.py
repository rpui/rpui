#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
'''








import json
import os
import glob
import shutil








print("Regenerating the gen files\n")

print("lang.json reading...", end='')
try:
    handle = open("../lang/lang.json", "r")
    lang = json.load(handle)
    handle.close()
    print("Ok")
except Exception as e:
    print(e)
    quit()


print("\nChecking directories...")
dirs = []
for l in lang.keys():
    dirname = lang[l]["id"]
    if l != "English":
        dirs.append(dirname)
    print(dirname,end="")
    if os.path.isdir("lang/"+dirname):
        print("...Ok")
    else:
        print("...Creating...",end="")
        try:
            os.mkdir("lang/"+dirname)
            print("Ok")
        except Exception as e:
            print(e)
            quit()


tws = []
for sfile in glob.glob('*.json'):
    tws.append(os.path.splitext(os.path.basename(sfile))[0])

print("\nProcessing gen files...")
dirs = ["en-us"]+dirs
for d in dirs:
    listcon = {"tw":{}}
    print(d)
    for tw in tws:
        print("\t"+tw, end="")
        sfile = tw+"gen/"+d+".json"
        tfile = "lang/"+d+"/"+tw+".json"
        if not os.path.isfile(sfile):
            sfile = tw+"/gen/en-us.json"
        try:
            handle = open(sfile, "r",encoding='utf-8')
            source = json.load(handle)
            handle.close()
            listcon["tw"][tw] = source["description"]
            shutil.copyfile(sfile, tfile)
        except ValueError:
            print(" broken JSON format ")
            exit()
        except:
            raise
        try:
            shutil.move(tw+"/static", "../../static/"+tw)
            print("...static moved", end="")
        except:
            pass
        print("...ok")
    print("\tList file...", end="")
    listfile = "lang/"+d+"/list.json"
    try:
        handle = open(listfile, 'w', encoding='utf-8')
        handle.write(json.dumps(listcon, indent=4, separators=(',', ': '),ensure_ascii=False))
        handle.close()
        print("ok")
    except:
        raise









