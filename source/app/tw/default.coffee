///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

define \
    ["dojo/_base/declare", 
    "dojo/_base/lang",
    "dojo/dom-construct",    
    "dijit/form/TextBox",
    "dijit/form/Button",
    "app/twbase",
    "dojo/domReady!"],(
        declare, 
        lang,
        domConstruct,
        TextBox,
        Button,
        twbase) ->







        declare "tw_default", [TWBase],
            



            constructor: (args) ->
                this.boxes = []
                for p in window.base.server_db.pool
                    if p.tid == this.defs.tid
                        this.vars = p.data




            postCreate:() ->
                if this.defs.title != ""
                    this.domNode.appendChild(domConstruct.create(
                        "label", {innerHTML:this.defs.title}))
                    this.domNode.appendChild(domConstruct.create("br"))
                
                for k of this.vars
                    v = this.vars[k]

                    label = domConstruct.create "label", 
                        {for:this.id+k, innerHTML:k+" :"}

                    input = new TextBox {
                        id: this.id+k
                        name: k,
                        value: v,
                        readOnly: Boolean(this.editable),
                        placeHolder: "...",
                        onInput: lang.hitch(this, "tw_change")}

                    this.boxes.push input

                    this.domNode.appendChild label 
                    this.domNode.appendChild input.domNode
                    this.domNode.appendChild(domConstruct.create("br"))
                    input.startup




            data:()-> 
                data = {}
                for k of this.vars
                    data[k] = eval(this.id+k+".value")
                return(data)




            update:(data)->
                if !this.is_writing()
                    for k of this.vars
                        try
                            obj = eval(this.id+k)
                            if data.hasOwnProperty(k)
                                obj.value = data[k]
                            else
                                obj.value = "error"
                        catch
                            console.log ("Data matching error")
