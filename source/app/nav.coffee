///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

define \
    ["dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/dom-construct",
    "dojo/Evented", 
    "dijit/layout/ContentPane",
    "dijit/form/Button",
    "dijit/_WidgetBase",
    "dojo/domReady!"],(
        declare, 
        lang,
        domConstruct,
        evented,
        ContentPane,
        Button,
        WidgetBase) ->





        declare "NavList", [ ContentPane, evented],
            



            constructor: (args) ->
                declare.safeMixin(this, args)




            postCreate: () ->
                if window.base.server_db.hasOwnProperty("uname")
                    name = window.base.server_db["uname"]
                    this.domNode.appendChild(domConstruct.create("a", {
                        innerHTML: window.base.locales.get("Home"),
                        href:"/"+name}))
                this.domNode.appendChild(domConstruct.create("br"))
              
            

            add_item: (text, callback) ->#, group="", enable=true) ->
                this.domNode.appendChild(domConstruct.create("br"))
                newitem = new Button({label: text, onClick:callback})
                this.addChild(newitem)
                return(newitem.id)