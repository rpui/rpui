///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

define \
    ["dojo/_base/declare",
    "dojo/request",
    "dojo/_base/lang",
    "dojo/dom-construct",
    "dojo/Evented", 
    "dijit/layout/ContentPane",
    "dijit/form/Button",
    "dijit/form/CheckBox",
    "dojo/store/Memory",
    "dijit/form/ComboBox",
    "dijit/_WidgetBase",
    "dijit/form/TextBox",
    "app/dockpaneconn",
    "dojo/domReady!"],(
        declare,
        request,
        lang,
        domConstruct,
        evented,
        ContentPane,
        Button,
        CheckBox,
        Memory,
        ComboBox,
        WidgetBase,
        TextBox,
        dockpaneconn) ->





        declare "DockPane", [ ContentPane, evented],




            constructor: (args) ->
                declare.safeMixin(this, args)
                this.region = "left"
                this.class = "contentitem"
                this.style += "padding:0px;"
                if this.hasOwnProperty("task")
                    for task in window.base.server_db.tasks
                        if task._id == this.tid
                            this.task = task
                    for pool in window.base.server_db.pool
                        if pool.tid == this.tid
                            this.vars = Object.keys(pool.data)




            postCreate: () ->
                menu = new ContentPane({
                    style: "padding:0px; border-bottom: 1px solid grey;"
                })
                movetoupbutton = new Button({
                    innerHTML: "<img src='/static/movetoup.png' class='dpbtnimg'></img>"
                    })
                movetodownbutton = new Button({
                    innerHTML: "<img src='/static/movetobottom.png' class='dpbtnimg'></img>"
                    })
                addtoupbutton = new Button({
                    innerHTML: "<img src='/static/addtoup.png' class='dpbtnimg'></img>"
                    })
                addtodownbutton = new Button({
                    innerHTML: "<img src='/static/addtobottom.png' class='dpbtnimg'></img>"
                    })
                closebutton = new Button({
                    innerHTML: "<img src='/static/close.png' class='dpbtnimg'></img>"
                    })
                menu.addChild(movetoupbutton)
                menu.addChild(movetodownbutton)
                menu.addChild(addtoupbutton)
                menu.addChild(addtodownbutton)
                menu.addChild(closebutton)

                movetoupbutton.onClick = lang.hitch(this, "moveup")
                movetodownbutton.onClick = lang.hitch(this, "movedown")
                addtoupbutton.onClick = lang.hitch(this, "addtoup")
                addtodownbutton.onClick = lang.hitch(this, "addtodown")
                closebutton.onClick = lang.hitch(this, "close")
                
                this.addChild(menu)
                
                
                #CENTER
                this.center = new ContentPane()
                this.addChild(this.center)

                
                #TITLE
                this.dock_title = new TextBox({value: this.title})
                title_label = domConstruct.create("label", {
                    for:this.dock_title.id, 
                    innerHTML:"Title : "})
                this.center.domNode.appendChild(title_label)
                this.center.addChild(this.dock_title)


                #EDITABLE
                this.dock_editable = new CheckBox({checked:this.editable})
                editable_label = domConstruct.create("label", {
                    for:this.dock_editable.id, 
                    innerHTML:"Editable : "})
                this.center.domNode.appendChild(editable_label)
                this.center.addChild(this.dock_editable)
                #this.dock_editable.onChange = lang.hitch(this, "change_editable")


                #TASKLIST
                tasklist_store = new Memory()
                taskname = ""
                for t in window.base.server_db.tasks
                    tasklist_store.add({name:t.name})##{name:t.name, id:..}
                    if t._id == this.tid
                        taskname = t.name
                this.tasklist_combo = new ComboBox({store:tasklist_store})
                tasklist_label = domConstruct.create("label", 
                    {for:this.tasklist_combo.id, innerHTML:"Task : "})
                this.center.domNode.appendChild(domConstruct.create("br"))
                this.center.domNode.appendChild(tasklist_label)
                this.center.addChild(this.tasklist_combo)
                this.tasklist_combo.onChange = lang.hitch(this, "change_task")


                #TASK WIDGET
                widgetlist_store = new Memory()
                widgetguiname = ""
                for w in Object.keys(window.twlist)
                    widgetlist_store.add({name:window.twlist[w].guiname, id:w})
                    if w == this.widget
                        widgetitem = {name:window.twlist[w].guiname, id:w}
                this.widgetlist_combo = new ComboBox({
                    store:widgetlist_store, 
                    disabled:false})
                widgetlist_label = domConstruct.create("label", 
                    {for:this.tasklist_combo.id, innerHTML:"Widget : "})
                this.center.domNode.appendChild(domConstruct.create("br"))
                this.center.domNode.appendChild(widgetlist_label)
                this.center.addChild(this.widgetlist_combo)
                this.widgetlist_combo.onChange = lang.hitch(this, "change_widget")

                #CONNECTIONS
                this.connections = new DockPaneConn({
                    vars:this.variables, 
                    cons:this.constants})
                this.addChild(this.connections)

                if this.tid != ""
                    this.tasklist_combo.set("value", taskname)
                    this.widgetlist_combo.set("item", widgetitem)




            change_task :(val) ->
                for t in window.base.server_db.tasks
                    if val == t.name
                        this.task = t
                        this.tid = t._id
                        this.widgetlist_combo.set("disabled", false)
                for v in window.base.server_db.pool
                    if v.tid == this.tid
                        this.connections.change_task(v)




            change_widget :(w) ->
                this.widget = this.widgetlist_combo.item.id
                if !window.twcache.hasOwnProperty(this.widget)
                    obj = this
                    f = "/app/tw/lang/"
                    f = f.concat(window.base.locales.get_current("id"))
                    f = f.concat("/"+this.widget+".json")
                    request(f, {handleAs:"json"}).then( 
                        (response) ->
                            window.twcache[obj.widget] = response
                            obj.connections.change_widget(window.twcache[obj.widget])
                        (err)->
                            console.log (err)
                    )
                else
                    this.connections.change_widget(window.twcache[this.widget])




            #change_editable: (v) ->
            #    console.log ( this.get_dock() )




            get_dock: () ->
                dock = {
                    tid : this.tid,
                    title : this.dock_title.value,
                    editable : this.dock_editable.checked,
                    variables : this.connections.get_connections(),
                    constants : this.connections.get_constants(),
                    widget : this.widget
                }
                return(dock)




            moveup :()->
                this.emit("onMoveUp", this)

            movedown :()->
                this.emit("onMoveDown", this)

            addtoup :()->
                this.emit("onAddUp", this)

            addtodown :()->
                this.emit("onAddDown", this)

            close :()->
                this.emit("onClose", this)