///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

require [
    "dojo/parser",
    "dojo/dom",
    "dojo/_base/declare", 
    "dojo/ready",
    "app/base",
    "dojo/domReady!" ], (
        parser,
        dom,
        declare,
        ready,
        base) ->



    ready(->



       declare "Index", [],




            constructor:()->
                null



            postCreate:()->
                total_nodes.textContent = window.base.server_db["nodes"]
                alive_nodes.textContent = window.base.server_db["alivenodes"]
                total_users.textContent = window.base.server_db["users"]




        base("index")
        page = new Index()

        initialized = ->
            page.postCreate()


        window.base.load_locales(initialized)

        )
