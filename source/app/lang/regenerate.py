#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
'''








import json
import os
import glob








print("Regenerating the language files\n")

print("lang.json reading...", end='')
try:
    handle = open("lang.json", "r")
    lang = json.load(handle)
    handle.close()
    print("Ok")
except Exception as e:
    print(e)
    quit()

print("\nChecking directories...")
dirs = []
for l in lang.keys():
    dirname = lang[l]["id"]
    if l != "English":
        dirs.append(dirname)
    print(dirname,end="")
    if os.path.isdir(dirname):
        print("...Ok")
    else:
        print("...Creating...",end="")
        try:
            os.mkdir(dirname)
            print("Ok")
        except Exception as e:
            print(e)
            quit()

totalitem = 0
untranslated = {}
for d in dirs:
    untranslated[d] = 0

print("\nMapping language files...")
for sfile in glob.glob('en-us/*.json'):
    print(sfile.split("/")[1])
    try:
        handle = open(sfile, "r", encoding='utf-8')
        source = json.load(handle)
        handle.close()
    except Exception as e:
        print(e)
        quit()
    for dirname in dirs:
        print("\t"+dirname+"...",end="")
        tfile = dirname+"/"+sfile.split("/")[1]
        newflag = False
        if not os.path.isfile(tfile):
            newflag = True
            print("Creating...",end="")
            try:
                handle = open(tfile, 'a', encoding='utf-8')
                handle.write(json.dumps(source, indent=4, separators=(',', ': ')))
                handle.close()
            except Exception as e:
                print(e)
                quit()
        if not newflag:
            try:
                handle = open(tfile, "r", encoding='utf-8')
                target = json.load(handle)
                handle.close()
            except Exception as e:
                print(e)
                quit()
            if sfile.split("/")[1] == "_dict.json":
                sections = ["dict"]
            else:
                sections = ["static", "dynamic"]
            for section in sections:
                for item in source[section]:
                    totalitem += 1
                    if (section == "dynamic" or section == "dict") and item not in target[section]:
                        target[section][item] = source[section][item]
                        print("+",end="")
                    if section == "static":
                        if list(source[section][item].keys())[0] != list(target[section][item].keys())[0]:
                            target[section][item] = source[section][item]
                            print("+",end="")
                    if target[section][item] == source[section][item]:
                        untranslated[dirname] += 1
            try:
                handle = open(tfile, "w", encoding="utf-8")
                handle.write(json.dumps(target, indent=4, separators=(',', ': '),ensure_ascii=False))
                handle.close()
            except Exception as e:
                print(e)
                quit()
        print("...Ok")

print("\nTotal items... "+str(totalitem))
print("\nUntranslated items")
for key in untranslated.keys():
    percent = 100-int((untranslated[key]*100)/totalitem)
    print(key, untranslated[key], "%"+str(percent))