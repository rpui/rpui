///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

define \
    ["dojo/_base/declare", 
    "dojo/_base/lang",
    "dojo/request",
    "dojo/dom-construct",
    "dojo/_base/window",
    "dojo/Evented", 
    "dijit/form/TextBox",
    "dijit/form/Button",
    "dijit/_WidgetBase",
    "dojo/domReady!"],(
        declare, 
        lang,
        request,
        domConstruct,
        win,
        evented,
        TextBox,
        Button,
        WidgetBase) ->


        MODE_READ = "read"
        MODE_WRITE = "write"



        declare "TWBase", [WidgetBase, evented],
            



            constructor: (args) ->
                declare.safeMixin(this, args)
                this._mode = MODE_READ
                this.sendbutton = true
                css = "<link rel='stylesheet' type='text/css' href='/app/tw/"
                css = css.concat(this.defs.widget+"/"+this.defs.widget+".css'>")
                domConstruct.place(css, win.body())
                




            buildRendering: () ->
                this.domNode = domConstruct.create("div")
            



            is_writing:()->
                if this._mode == MODE_WRITE
                    return(true)
                else
                    return(false)




            tw_change:(val) ->
                if !Boolean(this.editable) and this._mode == MODE_READ
                    this._mode = MODE_WRITE
                    if this.sendbutton
                        this.mode_write()
                    else
                        this.tw_send()                        





            tw_send:()->
                this.emit("onSend")
                



            tw_cancel:()->
                this._mode = MODE_READ
                this.mode_read()




            tw_data:()->
                d = this.data()
                this.tw_cancel()
                return(d)




            mode_write:()->
                this.sendButton = new Button {
                    label: "Send", 
                    onClick: lang.hitch(this, "tw_send")}
                this.cancelButton = new Button {
                    label: "Cancel", 
                    onClick: lang.hitch(this, "tw_cancel")}
                this.domNode.appendChild this.sendButton.domNode
                this.domNode.appendChild this.cancelButton.domNode




            mode_read:()->
                if this.sendbutton
                    this.cancelButton.destroyRecursive()
                    this.sendButton.destroyRecursive()
                    this.cancelButton.destroy()
                    this.sendButton.destroy()



            update:()->
                null