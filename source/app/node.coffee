///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

require [
    "dojo/ready",
    "dojo/_base/declare", 
    "dijit/registry",
    "app/base",
    "dojo/_base/lang",
    "app/server", 
    "app/msg",    
    "app/tasklist",
    "dojo/domReady!" ], (
        ready,
        declare,
        registry,
        base,
        lang,
        server,
        msg,        
        tasklist) ->



    ready(->




        declare "NodePage", [],



            constructor: () ->
                this.looper = new server(url="/xhr/node", repeat=true)
                this.looper.on("loaded", lang.hitch(this, "looper_loaded"))
                this.looper.on("error", lang.hitch(this, "looper_error"))
                this.looper.on("ontime", lang.hitch(this, "looper_call"))
                this.nid = new stack()
                this.nid.append(window.base.source, 
                    {"nid" : window.base.server_db["node"]["_id"]})

                this.sender = new server(url="/xhr/nodecommand")
                this.sender.on("loaded", lang.hitch(this, "sender_loaded"))
                this.sender.on("error", lang.hitch(this, "sender_error"))
                this.csource = Object.create(window.base.source)
                Object.assign(this.csource, window.base.source)
                this.csource["name"] = "command"





            postCreate:() ->
                window.base.navlist.add_item(
                    window.base.locales.get("EditDock"), 
                    lang.hitch(this, "edit"))
                this.w_tasklist = new TaskList(id:"w_tasklist")
                this.w_tasklist.on("onSend", lang.hitch(this, "send_command"))           
                w_mainStack_Content.addChild(this.w_tasklist)
                this.looper.start()




            looper_loaded: (response) ->
                this.w_tasklist.update(response)




            looper_error: (err) ->
                console.log (err)




            looper_call: () ->
                this.looper.run(this.nid)




            send_command: (cmds) ->
                c = new stack()
                c.append(this.csource, cmds.data())
                this.sender.run(c)




            sender_loaded: (response) ->
                null
                #console.log (response)




            sender_error: (err) ->
                console.log (err)



            edit: (c) ->
                location.href = "/u/editdock/"+window.base.server_db["node"]["name"]








        base("node")
        page = new NodePage()

        initialized = ->
            page.postCreate()

        window.base.load_locales(initialized)
        


        )
