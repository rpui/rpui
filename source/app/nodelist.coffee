///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

define \
    ["dojo/_base/declare", 
    "dojo/_base/lang",
    "dojo/dom-construct",
    "dijit/layout/ContentPane",
    "dojox/layout/TableContainer",
    "dijit/layout/LinkPane",
    "dijit/_WidgetBase",
    "app/server",
    "dojo/domReady!"],(
        declare, 
        lang,
        domConstruct,
        ContentPane,
        TableContainer,
        LinkPane,
        WidgetBase,
        server) ->





        declare "nodeitem", [ContentPane],
            



            constructor: (args) ->
                declare.safeMixin(this, args)
                this.region = "left"
                this.class = "contentitem"




            postCreate:() ->
                ref = window.base.server_db["user"]["uname"]+"/"+this.name
                editref = "/u/editnode/"+this.name
                n = this.name.charAt(0).toUpperCase()+this.name.slice(1)
                this.nameContent = new ContentPane({ class:"nameContent" })
                this.statusContent = new ContentPane({ class:"statusContent" })
                this.controlsContent = new ContentPane({ class:"controlsContent" })
                this.addChild(this.statusContent)
                this.addChild(this.nameContent)
                this.addChild(this.controlsContent)
                

                this.linkDom = domConstruct.create("a", {
                    href: ref, 
                    innerHTML: n})
                this.descDom = domConstruct.create("label", {
                    innerHTML: this.desc,
                    class: "textLow05"})
                this.statusDom = domConstruct.create("img", {
                    src: "/static/status-offline.png",
                    class: "statusImg"})
                this.editButtonDom = domConstruct.create("img", {
                    src: "/static/edit.png",
                    class: "controlsImg"})
                this.editLinkDom = domConstruct.create("a", {
                    href: editref, 
                    innerHTML: this.editButtonDom.outerHTML})

                this.nameContent.domNode.appendChild(this.linkDom)
                this.nameContent.domNode.appendChild(domConstruct.create("br"))
                this.nameContent.domNode.appendChild(this.descDom)
                this.statusContent.domNode.appendChild(this.statusDom)
                this.controlsContent.domNode.appendChild(this.editLinkDom)




            changeStatus:(status)->
                if status == "false" and this.status == "true"
                    this.statusDom.src = "/static/status-offline.png"
                if status == "true" and this.status == "false"
                    this.statusDom.src = "/static/status-online.png"
                this.status = status 








        declare "NewNodeItem", [ContentPane],
            



            constructor: (args) ->
                declare.safeMixin(this, args)
                this.region = "left"
                this.class = "newcontentitem"




            postCreate:() ->
                newLinkDom = domConstruct.create("a", {
                href: '/u/newnode', 
                innerHTML: window.base.locales.get("new_node")})
                this.domNode.appendChild(newLinkDom)








        declare "nodelist", [ContentPane],




            constructor: (args) ->
                declare.safeMixin(this, args)
                #this.nameDom = domConstruct.create("label", class:'bold', innerHTML:window.base.locales.get("node_title"))
                #this.hrDom = domConstruct.create("hr", style:'width:100%')
               


            
            load_nodes: () ->
                s = server("/xhr/nodes")
                s.on("loaded", lang.hitch(this, "_nodes_loaded"))
                #s.run()




            _nodes_loaded: (result) ->
                #for 
                    #test = new nodeitem({name:"Ali", desc:"Falan filan", status:false})
                    #this.domNode.appendChild(test.domNode)
                newone =  new NewNodeItem()
                this.domNode.appendChild(newone.domNode)




            postCreate:() ->
                #this.domNode.appendChild( this.nameDom)
                #this.domNode.appendChild(this.hrDom)
                for node in window.base.server_db["nodes"]
                    n = new nodeitem({name : node["name"], \
                        desc : node["desc"], \
                        id : node["_id"],\
                        status:"false"})
                    this.domNode.appendChild(n.domNode)
                newone =  new NewNodeItem()
                this.domNode.appendChild(newone.domNode)




            updateStatus:(nids) ->
                console.log (nids)
                for item in this.getChildren()
                    if "NewNodeItem_0" != item.id
                        console.log (item.id)
                        if item.id in nids
                            item.changeStatus("true")
                        else
                            item.changeStatus("false")
