///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

define [
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/cookie", 
    "dojo/domReady!"],(
        declare,
        lang,
        cookie) ->





        get_uri = (uname, nname, name) ->
            return(uname+"/"+nname+"/"+name)




        declare "stack",[],



            constructor: () ->
                this._stack = {}




            append: (source, data = {}) ->
                checklist = [
                    "id", 
                    "name", 
                    "uname",
                    "nname"]
                if typeof source == typeof {} && typeof data == typeof {}
                    for c in checklist
                        if ! source.hasOwnProperty(c)
                            return
                else
                    return
                s = Object.create(source)
                Object.assign(s, source)
                s["data"] = data
                id = get_uri(s["uname"], s["nname"], s["name"])
                if ! this.data(id)
                    this._stack[id] = s




            update: (uri, data) ->
                try
                    this._stack[uri]["data"] = data
                catch
                    return




            data: (uri) ->
                try
                    return(this._stack[uri]["data"])
                catch
                    return




            delete: (uri)->
                try
                    delete this._stack[uri]
                catch
                    return




            stack: ()->
                out = []
                for k in Object.keys(this._stack)
                    out.push(this._stack[k])
                return(out)








        declare "cmdata",[],
    



            constructor: (name="", data={})->
                this._data = {}
                if name != ""
                    this.cmd(name, data)




            cmd: (name, data)->
                this._data[name] = data




            remove: (name)->
                try
                    delete this._data[name]
                catch
                    return




            data: ()->
                out = {}
                for k in Object.keys(this._data)
                    out[k] = this._data[k]
                return(out)                








        declare "message",[],




            constructor: (stack)->
                this._msg = {
                    "uid" : "",
                    "nid" : "",
                    "uname" : window.base.source["uname"],
                    "nname" : window.base.source["nname"],
                    "_xsrf" : cookie("_xsrf"),
                    "stack" : stack.stack()
                }




            msg: ()->
                msg = this._msg
                msg["stack"] = JSON.stringify(this._msg["stack"])
                return(msg)



            
            json: ()->
                return(JSON.stringify(this._msg))



        #cmd = cmdata("up", {"ali":123, "veli":"deli"})
        #nodes.update("ali/admin/", {"fake":123})
        #nodes.delete("ali/admin/")
        #console.log ( cmd.data())
        #console.log (nodes.stack())
