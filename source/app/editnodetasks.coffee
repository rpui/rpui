///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

require [
    "dojo/ready",
    "dojo/request", 
    "dojo/_base/declare", 
    "dijit/registry",
    "app/base",
    "dojo/_base/lang",
    "dojo/dom-construct",
    "app/server", 
    "app/msg",    
    "app/docklist",
    "dojo/store/Memory",
    "dojo/dom",
    "dijit/form/Button",
    "dijit/form/CheckBox",
    "dijit/form/ValidationTextBox",
    "dojox/validate/web",
    "dojo/domReady!" ], (
        ready,
        request,
        declare,
        registry,
        base,
        lang,
        domConstruct,
        server,
        msg,        
        docklist,
        memory,
        dom,
        Button,
        CheckBox,
        ValidationTextBox,
        web) ->



    ready(->




        declare "EditNodeTasks", [],




            constructor:()->
                this.tasklist = window.base.server_db["tasks"]
                this.oldtname = ""
                this.xhrtasks = new server("")
                this.xhrtasks.on("loaded", lang.hitch(this, "saved"))
                this.xhrtasks.on("error", lang.hitch(this, "error"))




            postCreate:()->
                if window.base.server_err != ""
                    window.base.handle_messages("NodeNameLabel")
                dom.byId("TaskCancelLink").href = "/u/editnode/"+window.base.server_db["name"]
                
                NodeName.set("value", window.base.server_db["name"])
                TasksDelButton.onClick = lang.hitch(this, "task_del")
                TasksNewButton.onClick = lang.hitch(this, "task_new")
                Tasks.onChange = lang.hitch(this, "task_change")
                TaskSubmitButton.onClick = lang.hitch(this, "submit")
                access_options = [ 
                    { label: window.base.locales.get("Private"), value: "Private" },
                    { label: window.base.locales.get("Public"), value: "Public" }]
                TaskAccess.set("options", access_options)
                TaskShareAll.set("readOnly", true) 
                this.update_tasks()




            task_del: (c) ->
                for obj in Tasks.getSelected()
                    for t in Object.keys(this.tasklist)
                        if this.tasklist[t]["name"] == obj.value
                            delete this.tasklist[t]
                            console.log (this.tasklist)
                            obj.remove()
                this.update_tasks()
                this.task_cancel(this)




            task_new: (c) ->
                Tasks.set("disabled", true)
                TaskName.set("value", "")
                TaskDesc.set("value", "")
                TaskAccess.set("value", "Private")
                this.AddButton = new Button({
                    label: window.base.locales.get("Add"),
                    onClick : lang.hitch(this, "task_add")
                    })
                this.CancelButton = new Button({
                    label: window.base.locales.get("Cancel"),
                    onClick : lang.hitch(this, "task_cancel")
                    })
                TaskButtonsPane.destroyDescendants()
                TaskButtonsPane.addChild(this.AddButton)
                TaskButtonsPane.addChild(this.CancelButton)
                TaskName.validator = lang.hitch(this, "validate_new")
                TaskSubmitButton.set("disabled", true)




            validate_new:(v) ->
                if not /^[a-zA-Z][a-zA-z0-9_]*$/.test(v)
                    this.AddButton.set("disabled", true)
                    return(false)
                for t in Object.values(this.tasklist)
                    if t["name"] == v
                        this.AddButton.set("disabled", true)
                        return(false)
                this.AddButton.set("disabled", false)
                return(true)
            



            validate_exist:(v) ->
                if not /^[a-zA-Z][a-zA-z0-9_]*$/.test(v)
                    this.UpdateButton.set("disabled", true)
                    return(false)
                for t in Object.values(this.tasklist)
                    if t["name"] == v and v != this.oldtname
                        this.UpdateButton.set("disabled", true)
                        return(false)
                this.UpdateButton.set("disabled", false)
                return(true)




            task_add: (c) ->
                TaskName.validator = ()->
                    return(true)
                if TaskName.isValid
                    newtask = {
                        name : TaskName.value,
                        desc : TaskDesc.value,
                        access : TaskAccess.value,
                        share : [],
                        _id : ""
                    }
                    this.tasklist.push(newtask)
                    TaskButtonsPane.destroyDescendants()
                    TaskName.set("value", "")
                    TaskDesc.set("value", "")
                    TaskAccess.set("value", "Private")
                    this.update_tasks()
                    Tasks.set("disabled", false)
                    TaskSubmitButton.set("disabled", false)




            task_cancel: (c) -> 
                TaskName.validator = ()->
                    return(true)
                TaskName.set("value", "")
                TaskDesc.set("value", "")
                TaskAccess.set("value", "Private")
                Tasks.set("disabled", false)
                TaskButtonsPane.destroyDescendants()
                TaskSubmitButton.set("disabled", false)




            update_tasks: ()->
                Tasks.domNode.innerHTML = ""
                for t in Object.values(this.tasklist)
                    Tasks.domNode.appendChild( domConstruct.create("option", {
                        innerHTML:t["name"]}))
                Tasks.set("size", Tasks.containerNode.options.length)
                




            task_change: (c) ->
                if c.length == 1
                    this.oldtname = c[0]
                    for t in Object.values(this.tasklist)
                        if t["name"] == c[0]
                            TaskName.set("value", t["name"])
                            TaskDesc.set("value", t["desc"])
                            TaskAccess.set("value", t["access"])
                    if TaskButtonsPane.domNode.children.length == 0
                        this.UpdateButton = new Button({
                            label: window.base.locales.get("Update"),
                            onClick : lang.hitch(this, "task_update")
                            })
                        TaskButtonsPane.addChild(this.UpdateButton)
                    TaskName.validator = lang.hitch(this, "validate_exist")




            task_update:(c)->
                TaskName.validator = ()->
                    return(true)
                for t in Object.values(this.tasklist)
                    if t["name"] == this.oldtname
                        t["name"] = TaskName.value 
                        t["desc"] = TaskDesc.value
                        t["access"] = TaskAccess.value
                TaskButtonsPane.destroyDescendants()
                TaskName.set("value", "")
                TaskDesc.set("value", "")
                TaskAccess.set("value", "Private")
                this.update_tasks()




            submit:(c) ->
                data = new stack()
                tlist = new Array()
                for t in Object.values(this.tasklist)
                    if t != null
                        tlist.push(t)
                data.append(window.base.source, 
                    {"tasks": tlist, "nid":window.base.server_db["id"]})
                this.xhrtasks.run(data)



            saved: (r) ->
                try
                    window.base.server_err = r[0]["data"]["error"]
                    window.base.handle_messages("MsgArea")
                catch
                    window.location = "/"+window.base.server_db["uname"]




            error: (e) ->
                window.base.server_err = "NotUpdated"
                window.base.handle_messages("MsgArea")
                console.log (e)





            
        base("editnodetasks")
        page = new EditNodeTasks()

        initialized = ->
            page.postCreate()


        window.base.load_locales(initialized)
        


        )
