///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

require [
    "dojo/ready",
    "dojo/request", 
    "dojo/cookie",
    "dojo/_base/declare", 
    "dijit/registry",
    "app/base",
    "dojo/_base/lang",
    "dojo/dom-construct",
    "app/server", 
    "app/msg",    
    "app/docklist",
    "dojox/validate/_base"
    "dojo/dom",
    "dijit/form/Button",
    "dijit/form/ValidationTextBox",
    "dojox/validate/web",
    "dojo/domReady!" ], (
        ready,
        request,
        cookie
        declare,
        registry,
        base,
        lang,
        domConstruct,
        server,
        msg,        
        docklist,
        validation,
        dom,
        Button,
        ValidationTextBox,
        web) ->



    ready(->




        declare "UPassword", [],




            constructor:()->
                null




            postCreate:()->
                Cancel.href = "/"+window.base.server_db["uname"]
                SubmitButton.onClick = lang.hitch(this, "submit")
                NewPass1.validator = lang.hitch(this, "new_pass_check")
                NewPass2.validator = lang.hitch(this, "new_pass_check")
                if window.base.server_db["code"] != ""
                    Password.set("disabled", true)






            new_pass_check:() ->
                if NewPass1.value == NewPass2.value and NewPass1.value != Password.value
                    NewPass1.set("state","Normal")
                    NewPass2.set("state","Normal")
                    return(true)
                NewPass1.set("state","Error")
                NewPass2.set("state","Error")
                return(false)




            submit:(c) ->
                if NewPass1.isValid() and NewPass2.isValid()
                    data = new stack()
                    data.append(window.base.source, {
                        code : window.base.server_db["code"],
                        passw : Password.value,
                        newpassw : NewPass1.value})
                    s = new server("/xhr/uchangepassword")
                    s.on("loaded", lang.hitch(this, "result"))
                    s.on("error", lang.hitch(this, "error"))
                    s.run(data)




            result:(respond) ->
                try
                    construct.destroy("pmessage")
                catch
                    null
                if respond[0]["data"].hasOwnProperty("fail")
                    window.base.server_err = respond[0]["data"]["fail"]
                    window.base.handle_messages(MsgArea.id, pos ="before", persistent ="true")
                if respond[0]["data"].hasOwnProperty("success") 
                    window.base.server_msg = "Changed"
                    window.base.server_err = null
                    window.base.handle_messages(MsgArea.id, pos ="before", persistent ="true")
                    Cancel.textContent = window.base.locales.get("Ok")
                    Cancel.href = "/login"
                    SubmitButton.onClick = ""
                    



            error:(e) ->
                console.log e
                window.base.server_err = "ServerError"
                window.base.handle_messages(MsgArea.id)






            
        base("upassword")
        page = new UPassword()

        initialized = ->
            page.postCreate()


        window.base.load_locales(initialized)
        


        )
