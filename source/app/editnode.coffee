///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

require [
    "dojo/ready",
    "dojo/request", 
    "dojo/_base/declare", 
    "dijit/registry",
    "app/base",
    "dojo/_base/lang",
    "dojo/dom-construct",
    "app/server", 
    "app/msg",    
    "app/docklist",
    "dojox/validate/_base"
    "dojo/dom",
    "dijit/form/Button",
    "dijit/form/ValidationTextBox",
    "dojox/validate/web",
    "dojo/domReady!" ], (
        ready,
        request,
        declare,
        registry,
        base,
        lang,
        domConstruct,
        server,
        msg,        
        docklist,
        validation,
        dom,
        Button,
        ValidationTextBox,
        web) ->



    ready(->




        declare "EditNode", [],




            constructor:()->
                this.xhrremove = new server("/xhr/removenode")
                this.xhrremove.on("loaded", lang.hitch(this, "removed"))
                this.xhrremove.on("error", lang.hitch(this, "not_removed"))





            postCreate:()->
                if window.base.server_err != ""
                    window.base.handle_messages("NodeNameLabel")
                this.oldname = dom.byId("OldName")
                access_options = [ 
                    { label: window.base.locales.get("Private"), value: "Private" },
                    { label: window.base.locales.get("Public"), value: "Public" }]
                NodeAccess.set("options", access_options)
                NodeAccess.onChange = lang.hitch(this, "access_changed")
                NodeName.validator = this.is_name_exist
                NodeAccess.set("value", "Private")
                dom.byId("NodeCancelLink").href = "/"+window.base.server_db["uname"]

                if window.base.server_db["mode"] == "edit"
                    dom.byId("FollowedEdit").href = "/u/editnodefolloweds/"+window.base.server_db["name"]
                    dom.byId("TasksEdit").href = "/u/editnodetasks/"+window.base.server_db["name"]
                    this.oldname["value"] = window.base.server_db["name"]
                    NodeName.set("value", window.base.server_db["name"])
                    NodeDesc.set("value", window.base.server_db["desc"])
                    NodeAccess.set("value", window.base.server_db["access"])
                    this.removedata = new stack()
                    this.removedata.append(window.base.source, {"nname":window.base.server_db["name"]})
                    window.base.navlist.add_item(
                        window.base.locales.get("Remove"), 
                        lang.hitch(this, "remove"))
                
                if window.base.server_db["mode"] == "new"
                    dom.byId("FollowedEdit").remove()
                    dom.byId("TasksEdit").remove()
                #NEEDS DEVELOPMENT
                NodeShareAll.set("readOnly", true) 




            is_name_exist: (v) ->
                if not validation.isText(v, {minlength: 3, maxlength: 12})
                    NodeName.set('invalidMessage', window.base.locales.get("NodeNameInvalid"))
                    return(false)
                if not /^[a-zA-Z][a-zA-z0-9_]*$/.test(v)
                    NodeName.set('invalidMessage', 
                        window.base.locales.get("NodeNameInvalid"))
                    return(false)

                if window.base.server_db["mode"] == "new"
                    if v in window.base.server_db["nodes"]
                        NodeName.set('invalidMessage', window.base.locales.get("NodeNameExist"))
                        return(false)
                else
                    if v in window.base.server_db["nodes"]
                        if dom.byId("OldName").value != v
                            NodeName.set('invalidMessage', window.base.locales.get("NodeNameExist"))
                            return(false)
                return(true)




            access_changed: (v) ->
                if v == "Private"
                    NodeShareAll.set("disabled", true)
                if v == "Public"
                    NodeShareAll.set("disabled", false)




            remove: (c) ->
                RemoveQuestionArea = dom.byId("RemoveQuestionArea")
                w_mainStack.selectChild(w_mainStack_Content)
                if window.base.server_db["mode"] == "edit"
                    RemoveQuestionArea.appendChild( 
                        domConstruct.create("label", {
                            innerHTML:window.base.locales.get("RemoveQuestion")}))
                    RemoveQuestionArea.appendChild(domConstruct.create("br"))
                    yesbutton = new Button({
                        label: window.base.locales.get("Yes"),
                        onClick : lang.hitch(this, "do_remove")
                        })
                    RemoveQuestionArea.appendChild(yesbutton.domNode)
                    cancelbutton = new Button({
                        label: window.base.locales.get("Cancel"),
                        onClick : lang.hitch(this, "cancel_remove")
                        })
                    RemoveQuestionArea.appendChild(cancelbutton.domNode)
                    RemoveQuestionArea.appendChild(domConstruct.create("br"))
                    RemoveQuestionArea.appendChild(domConstruct.create("br"))


            

            do_remove: (c) ->
                domConstruct.destroy("NodeSubmitButton")
                domConstruct.destroy("FollowedEdit")
                domConstruct.destroy("TasksEdit")
                RemoveQuestionArea.destroyDescendants()
                this.xhrremove.run(this.removedata)



            cancel_remove: (c) ->
                RemoveQuestionArea.destroyDescendants()




            removed: (respond) ->
                if respond[0]["data"].hasOwnProperty("fail")
                    RemoveQuestionArea = dom.byId("RemoveQuestionArea")
                    window.base.server_err = "NotDeleted"
                    window.base.handle_messages(RemoveQuestionArea.id)
                    RemoveQuestionArea.appendChild( 
                        domConstruct.create("a", {
                            href:"/"+window.base.server_db["uname"]
                            innerHTML:window.base.locales.get("Back")}))
                else
                    location.href = "/"+window.base.server_db["uname"]




            not_removed: (e) ->
                console.log e
                RemoveQuestionArea = dom.byId("RemoveQuestionArea")
                window.base.server_err = "NotDeleted"
                window.base.handle_messages(RemoveQuestionArea.id)
                RemoveQuestionArea.appendChild( 
                    domConstruct.create("a", {
                        href:"/"+window.base.server_db["uname"]
                        innerHTML:window.base.locales.get("Back")}))







            
        base("editnode")
        page = new EditNode()

        initialized = ->
            page.postCreate()


        window.base.load_locales(initialized)
        


        )
