///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

require [
    "dojo/ready",
    "dojo/request", 
    "dojo/_base/declare", 
    "dijit/registry",
    "app/base",
    "dojo/_base/lang",
    "dojo/dom-construct",
    "app/server", 
    "app/msg",    
    "app/docklist",
    "dojox/validate/_base"
    "dojo/dom",
    "dijit/form/Button",
    "dijit/form/ValidationTextBox",
    "dojox/validate/web",
    "dojo/domReady!" ], (
        ready,
        request,
        declare,
        registry,
        base,
        lang,
        domConstruct,
        server,
        msg,        
        docklist,
        validation,
        dom,
        Button,
        ValidationTextBox,
        web) ->



    ready(->




        declare "USettings", [],




            constructor:()->
                null




            postCreate:()->
                db = window.base.server_db["user"]
                Rcode.textContent = db["rcode"]
                Uname.textContent = db["uname"]
                FirstName.set("value", db["fname"])
                LastName.set("value", db["lname"])
                Email.set("value", db["email"])
                Cancel.href = "/"+window.base.server_db["uname"]
                Lang.set("options", window.base.locales.forselect)
                Lang.set("value", db["lang"])

                Email.validator = lang.hitch(this, "check_email")
                Email.checkedvalue = db["email"]
                Email.checkedstate = "true"

                SubmitButton.onClick = lang.hitch(this, "submit")




            check_email:(v, c) ->
                if window.base.server_db["user"]["email"] != v
                    if not validation.isEmailAddress(v)
                        Email.checkedstate = "Error"
                        Email.set('invalidMessage', 
                            window.base.locales.get("InvalidEmail"))
                        return(false)
                    if not Email.focused
                        if v != Email.checkedvalue
                            Email.checkedvalue = v
                            data = new stack()
                            data.append(window.base.source, {"email": v})
                            s = new server("/xhr/signupcheck")
                            s.on("loaded", lang.hitch(this, "email_check_result"))
                            s.on("error", lang.hitch(this, "email_check_error"))
                            s.run(data)
                    if Email.checkedstate == "Error" 
                        return(false)
                    else
                        return(true)
                else
                    return(true)




            email_check_result: (respond) ->
                try
                    if respond[0]["data"]["result"] == false
                        Email.set("state","")
                        Email.checkedstate = ""
                    else
                        Email.set("state","Error")
                        Email.checkedstate = "Error"
                        Email.set('invalidMessage', 
                            window.base.locales.get("RegisteredEmail"))
                catch   
                    Email.set("state","Error")
                    Email.checkedstate = "Error"




            email_check_error: (e) ->
                console.log (e)
                Email.set("state","Error")
                Email.checkedstate = "Error"
                Email.set('invalidMessage', 
                    window.base.locales.get("InvalidEmail"))




            submit:(c) ->
                if FirstName.isValid() and LastName.isValid() and Email.isValid() and Password.isValid()
                    data = new stack()
                    data.append(window.base.source, {
                        uname : window.base.server_db["uname"],
                        fname : FirstName.value,
                        lname : LastName.value,
                        email : Email.value,
                        lang : Lang.value,
                        passw : Password.value})
                    s = new server("/xhr/usettingschange")
                    s.on("loaded", lang.hitch(this, "result"))
                    s.on("error", lang.hitch(this, "error"))
                    s.run(data)




            result:(respond) ->
                if respond[0]["data"].hasOwnProperty("fail")
                    window.base.server_err = respond[0]["data"]["fail"]
                    window.base.handle_messages(MsgArea.id)
                else
                    location.href = "/"+window.base.server_db["uname"]




            error:(e) ->
                console.log e
                window.base.server_err = "ServerError"
                window.base.handle_messages(MsgArea.id)






            
        base("usettings")
        page = new USettings()

        initialized = ->
            page.postCreate()


        window.base.load_locales(initialized)
        


        )
