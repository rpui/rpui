///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

define [
    "dojo/_base/declare",
    "dojo/cookie",
    "dojo/dom-construct", 
    "dojo/_base/window",
    "dojo/on",  
    "dojo/_base/lang",  
    "dojo/has",
    "dojo/sniff", 
    "dojo/domReady!"],(
        declare,
        cookie,
        construct,
        win,
        event,
        lang,
        has,
        sniff) ->



        
        desktop = "desktop"
        mobile = "mobile"
        tv = "tv"




        declare "viewport",[],




            constructor: () ->
                if (! cookie("viewmode") )
                    this._mode = this._discover_mode()
                    cookie("viewmode", this._mode, 
                        {"expires" : "Fri, 31 Dec 9999 23:59:59 GMT"})
                else
                    this._mode = cookie("viewmode")

                link = "<link rel='stylesheet' type='text/css' href='/app/css/"
                link = link.concat(this._mode).concat("/")
                link = link.concat(this._mode).concat(".css'>")
                construct.place(link, win.body())
                console.log("View mode: ".concat(this._mode))




            _discover_mode: () ->
                if has("android") or has("ios")
                    return(mobile)
                else
                    return(desktop)



            mode:()->
                return(this._mode)