///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

define \
    ["dojo/_base/declare", 
    "dojo/request", 
    "dojo/_base/config",
    "dojo/cookie",
    "dojo/store/Memory", 
    "dojo/_base/lang",
    "dojo/_base/kernel",
    "dojox/lang/functional/object",
    "dijit/registry",
    "dojo/dom"],(
        declare, 
        request, 
        config,
        cookie,
        memory,
        lang,
        kernel,
        func,
        registry,
        dom) ->




        declare "i18n",[],




            _locale_path : "/app/lang/"
            _default_locale : "en-us"




            constructor: (fn, callback, error) ->
                this._callback = callback
                this._error = error
                this._fn = fn.concat(".json")
                this._lang = {}
                this._locales = {}
                this._dict = {}
                this._base = {}
                this.native = []
                this.forselect = []
                this._current = ""
                obj = this


                #First get language database
                f = this._locale_path.concat("lang.json")
                request(f, {handleAs:"json"}).then( 
                    (response) ->
                        obj._lang = response
                        keys = func.keys(obj._lang)
                        for key in keys
                            obj.native.push( obj._lang[key] )
                            obj.forselect.push({
                                value: obj._lang[key]["id"], 
                                label: obj._lang[key]["native"]})
                        obj._lang_loaded()
                    (err)->
                        obj._error(f)
                    )



            change: (n) ->
                this._callback = null
                for k in Object.keys(this._lang)
                    if this._lang[k]["native"] == n
                        this._load_locale(k)



            
            _lang_loaded: () ->
                #Then check cookie
                lang = this._lang
                locale = ""
                if cookie("user-locale")
                    #Check db for cookie
                    locale = key for key of lang when lang[key]["id"] is cookie("user-locale")
                else
                    #Check db for browser locale
                    locale = key for key of lang when lang[key]["id"] is kernel.locale

                if locale == ""
                    locale = key for key of lang when lang[key]["id"] is this._default_locale

                this._current = locale
                console.log ("Locale : ".concat(locale))
                this._load_locale(locale)


            

            _load_locale: (key) ->
                #Let's load locale file 
                obj = this
                f = this._locale_path.concat(this._lang[key]["id"])
                d = f.concat("/_dict.json")
                b = f.concat("/base.json")
                f = f.concat("/").concat(this._fn)
                
                request(d, {handleAs:"json"}).then( 
                    (response) ->
                        obj._dict = response
                        request(b, {handleAs:"json"}).then( 
                            (response) ->
                                obj._base = response
                                request(f, {handleAs:"json"}).then( 
                                    (response) ->
                                        obj._locales = response
                                        obj._current = key
                                        cookie("user-locale", obj._lang[key]["id"])
                                        obj._locales["dynamic"] = Object.assign(
                                            obj._locales["dynamic"], obj._base["dynamic"])
                                        obj._locales["static"] = Object.assign(
                                            obj._locales["static"], obj._base["static"])
                                        obj._mix_dict()
                                        obj._set_statics()
                                    (err)->
                                        obj._error(f)
                                    )
                            (err) ->
                                obj._error(b)
                            )        
                    (err)->
                        obj._error(d)
                    )





            _mix_dict:() ->
                find =(s)->
                    hit = s.search("@:")
                    last = s.slice(hit).search(" ")
                    if last == -1
                        last = s.length
                    return([hit, last, s.slice(hit+2, last)])

                repl = (s, pos, c)->
                    return(s.replace("@:"+pos[2],c))

                st = this._locales["static"]
                dy = this._locales["dynamic"]
                for key in func.keys(st)
                    v = func.values(st[key])[0]
                    pos = find(v)
                    if pos[0] > -1
                        k2 = func.keys(this._locales["static"][key])[0]
                        this._locales["static"][key][k2] = repl(v, pos, this._dict["dict"][pos[2]])
                for key in func.keys(dy)
                    v = dy[key]
                    pos = find(v)
                    if pos[0] > -1
                        this._locales["dynamic"][key] = repl(v, pos, this._dict["dict"][pos[2]])





            _set_statics:() ->
                #Decorate static texts
                st = this._locales["static"]
                keys = func.keys(st)
                for key in keys
                    do (key) ->
                        attr = func.keys(st[key])[0]
                        if registry.byId(key)
                            registry.byId(key).set(attr, st[key][attr])
                        else if dom.byId(key)
                            dom.byId(key)[attr] = st[key][attr]
                this._callback()



            get:(key)->
                #Get dynamic texts
                #Example:
                #  window.base.locales.get("test")
                if this._locales["dynamic"][key]
                    return(this._locales["dynamic"][key])
                else
                    return("")



            # key, native or id
            get_current: (c = "key") ->
                if c == "native"
                    return( this._lang[this._current]["native"])
                else if c == "id"
                    return( this._lang[this._current]["id"])
                else
                    return(this._current)
