///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

require [
    "dojo/ready",
    "dojo/request", 
    "dojo/_base/declare", 
    "dijit/registry",
    "app/base",
    "dojo/_base/lang",
    "app/server", 
    "app/msg",    
    "app/docklist",
    "dojo/domReady!" ], (
        ready,
        request,
        declare,
        registry,
        base,
        lang,
        server,
        msg,        
        docklist) ->



    ready(->




        declare "EditDockPage", [],




            constructor:()->
                window.twcache = {}




            postCreate:()->
                obj = this
                f = "/app/tw/lang/"
                f = f.concat(window.base.locales.get_current("id"))
                f = f.concat("/list.json")
                request(f, {handleAs:"json"}).then( 
                    (response) ->
                        window.twlist = response.tw
                        obj.dList = new DockList()
                        w_mainStack_Content.addChild(obj.dList)
                        w_mainStack_Content.resize()
                    (err)->
                        console.log (err)
                    )







            
        base("editdock")
        page = new EditDockPage()

        initialized = ->
            page.postCreate()


        window.base.load_locales(initialized)
        


        )
