///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

define \
    ["dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/dom-construct",
    "dojo/Evented", 
    "dijit/layout/ContentPane",
    "dijit/form/Button",
    "dijit/form/ComboBox",
    "dojo/store/Memory",
    "dijit/_WidgetBase",
    "dojo/domReady!"],(
        declare, 
        lang,
        domConstruct,
        evented,
        ContentPane,
        Button,
        ComboBox,
        Memory,
        WidgetBase) ->





        declare "UserMenu", [ ContentPane, evented],
            



            constructor: (args) ->
                declare.safeMixin(this, args)

              
            

            postCreate: () ->
                store = Memory({ data : window.base.locales.native })
                this.ulangCombo = new ComboBox({store:store, searchAttr:"native"})
                this.ulangCombo.set("value", window.base.locales.get_current())
                this.ulangCombo.onChange = lang.hitch(this, "change_lang")
                this.addChild(this.ulangCombo)

                if window.base.server_db.hasOwnProperty("uname")
                    name = window.base.server_db["uname"]
                    name = name.charAt(0).toUpperCase()+name.slice(1)
                    this.domNode.appendChild(domConstruct.create("br"))
                    this.domNode.appendChild(domConstruct.create("br"))
                    this.domNode.appendChild(domConstruct.create("label", {
                        textContent: window.base.locales.get("Welcome")+" "+name}))
                    this.domNode.appendChild(domConstruct.create("br"))
                    this.domNode.appendChild(domConstruct.create("br"))
                    this.domNode.appendChild(domConstruct.create("a", {
                        innerHTML: window.base.locales.get("UserSettings"),
                        href:"/u/settings"}))
                    this.domNode.appendChild(domConstruct.create("br"))
                    this.domNode.appendChild(domConstruct.create("br"))
                    this.domNode.appendChild(domConstruct.create("a", {
                        innerHTML: window.base.locales.get("ChangePass"),
                        href:"/u/changepassword"}))
                    this.domNode.appendChild(domConstruct.create("br"))
                    this.domNode.appendChild(domConstruct.create("br"))




            add_item: (text, callback, obj) ->#, group="", enable=true) ->
                this.domNode.appendChild(domConstruct.create("br"))
                newitem = new Button({label: text, onClick:callback})
                this.addChild(newitem)




            change_lang: (v) ->
                if window.base.locales.get_current("native") != v
                    window.base.locales.change(v)