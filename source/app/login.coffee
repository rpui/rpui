///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

require [ "dojo/dom",
    "dojo/dom-construct",
    "dojo/json",
    "dojo/parser", 
    "dojo/_base/event",
    "dojo/_base/lang", 
    "dojo/on", 
    "dijit/registry",
    "dojox/validate/_base",
    "dojo/date/stamp",
    "dojo/store/Memory",
    "dojo/ready",
    "app/base",    
    "dojo/domReady!" ], (
        dom,
        construct,
        json, 
        parser, 
        bevent, 
        lang, 
        _on, 
        registry, 
        validation, 
        stamp, 
        memory, 
        ready, 
        base) ->



    ready(->

        base("login")




        initialized = ->

            #l_topPane.destroyRecursive()


            on_change_forgot = (s)->
                if s == true
                    registry.byId("loginPassword").set("required",false)
                else
                    registry.byId("loginPassword").set("required",true)


            registry.byId("loginForgotPassw").onChange = on_change_forgot
            window.base.handle_messages("loginEmailLabel")
            


        window.base.load_locales(initialized)

        )
