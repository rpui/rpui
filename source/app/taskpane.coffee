///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

define \
    ["dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/dom-construct",
    "dojo/Evented",
    "dojo/request", 
    "dijit/layout/ContentPane",
    "dojox/layout/TableContainer",
    "dijit/layout/LinkPane",
    "dijit/_WidgetBase",
    "app/msg",    
    "dojo/domReady!"],(
        declare, 
        lang,
        domConstruct,
        evented,
        request,
        ContentPane,
        TableContainer,
        LinkPane,
        WidgetBase,
        msg) ->





        declare "TaskPane", [ContentPane, evented],
            



            constructor: (args) ->
                declare.safeMixin(this, args)
                tw = "tw/"+this.defs["widget"]
                this.region = "left"
                this.class = "contentitem"
                this.source = {
                    "id" : this.task["_id"], 
                    "name" : this.task["name"], 
                    "uname" : window.base.server_db["uname"],
                    "nname" : this.nname}
                try
                    require [tw], lang.hitch(this, "_constructor")
                catch
                    null
              
            


            _constructor:(tw) ->
                obj = this
                s = "/app/tw/"
                s = s.concat(this.defs.widget+".json")
                request(s, {handleAs:"json"}).then( 
                    (response) ->
                        if response.rpuivers.hasOwnProperty(window.base.version)
                            subv = response.rpuivers[window.base.version]
                            if subv.indexOf(window.base.subversion) != -1
                                obj.ids = response
                                f = "/app/tw/lang/"
                                f = f.concat(window.base.locales.get_current("id"))
                                f = f.concat("/"+obj.defs.widget+".json")
                                request(f, {handleAs:"json"}).then( 
                                    (response) ->
                                        parms = {twdata: response, defs: obj.defs, task:obj.task}
                                        obj.widget = eval("new tw_"+obj.defs["widget"]+"(parms)")
                                        obj.domNode.appendChild(obj.widget.domNode)
                                        obj.widget.on("onSend", lang.hitch(obj, "_onSend"))
                                    (err)->
                                        console.log (err)
                                )
                            else
                                console.log ("Incompatible tw subversion : "+obj.defs.widget)
                        else
                            console.log ("Incompatible tw version : "+obj.defs.widget)
                    (err)->
                        console.log (err)
                )




            _onSend:()->
                this.emit("onSend")




            getData:()->
                if !this.widget.is_writing()
                    return(false)
                try
                    return( this.widget.tw_data() )
                catch e
                    console.log ("tw error: "+e)





            updateData:(data)->
                if typeof this.widget != undefined
                    this.widget.update(data["data"]["data"])


