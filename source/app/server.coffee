///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

define \
    ["dojo/_base/declare", 
    "dojo/Evented", 
    "dojo/on", 
    "dojo/request/xhr", 
    "dojo/_base/lang", 
    "dojo/cookie", 
    "dojox/timing"
    "app/msg",    
    "dojo/domReady!"],(
        declare, 
        evented, 
        event, 
        xhr, 
        lang, 
        cookie,
        timing,
        msg) ->




        declare "server",[evented],




            constructor: (url="/xhr", repeat=false, method="POST") ->
                this.url = url
                this.method = method
                this.headers = { 'Content-Type': 'text/plain' }
                this.repeat = repeat
                this.timer = new timing.Timer(1000)
                this.timer.onTick = lang.hitch(this, "_tick")
                this.preXhr = 0
                this.xhrLatency = 0
                this.minDelay = 3000
                #"start", "send", "load", "error", "done", "stop".
                #notify("load", this.loaded)
                #notify("error", this.error)
                #console.log(this)





            run: ( data ) ->
                d = new message(data)
                #data["_xsrf"] = cookie("_xsrf")
                this.preXhr = new Date().getTime()
                xhr(this.url, {
                    data: d.msg(),
                    method: this.method
                    }).then( lang.hitch(this, "loaded"), lang.hitch(this, "error"))




            _tick: () ->
                passed = new Date().getTime() - this.preXhr
                wait = this.xhrLatency + this.minDelay
                if passed >= wait
                    this.emit("ontime")




            loaded: (response) ->
                this.xhrLatency = new Date().getTime() - this.preXhr
                try
                    r = JSON.parse(response).stack
                catch
                    window.base.server_err = response
                    this.emit("error", response)
                    return
                this.emit("loaded", r)




            error: (errmsg) ->
                this.xhrLatency = new Date().getTime() - this.preXhr
                this.emit("error", errmsg)

            


            start: () ->
                if this.repeat == true
                    this.timer.start()




            stop: () ->
                if this.repeat == true
                    this.timer.stop()
