///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

define \
    ["dojo/_base/declare",
    "dojo/request",
    "dojo/_base/lang",
    "dojo/dom-construct",
    "dojo/Evented", 
    "dijit/layout/ContentPane",
    "dijit/form/Button",
    "dijit/form/CheckBox",
    "dojo/store/Memory",
    "dijit/form/ComboBox",
    "dijit/_WidgetBase",
    "dijit/form/TextBox",
    "dojo/domReady!"],(
        declare,
        request,
        lang,
        domConstruct,
        evented,
        ContentPane,
        Button,
        CheckBox,
        Memory,
        ComboBox,
        WidgetBase,
        TextBox) ->





        declare "DockPaneConn", [ContentPane, evented],




            constructor: (args) ->
                declare.safeMixin(this, args)
                this.task = {}
                this.widget = {}
                this.connections = {}
                this.constants = {}




            #postCreate: () ->
            #    null




            change_task: (v) ->
                this.task = v
                this.store = new Memory()
                for d in Object.keys(this.task.data)
                    this.store.add({name:d})
                for c in Object.keys(this.connections)
                    this.connections[c].store = this.store
                    this.connections[c].set("value", "")




            change_widget: (w) ->
                this.widget = w
                this.destroyDescendants()
                this.connections = {}
                this.constants = {}
                for d in Object.keys(this.widget.variables)
                    this.new_connection(d)
                for s in Object.keys(this.widget.constants)
                    this.new_constant(s)




            new_connection: (name) ->
                if this.connections != {}
                    this.domNode.appendChild(domConstruct.create("br"))
                combo = new ComboBox({store:this.store})
                label = domConstruct.create("label", 
                    {for:combo.id, innerHTML: name+" : "})
                this.domNode.appendChild(label)
                this.addChild(combo)
                #combo.onChange = lang.hitch(this, "change_conn")
                this.connections[name] = combo
                if this.vars.hasOwnProperty(name) and 
                        this.store.query({name:this.vars[name]}).length
                    combo.set("value", this.vars[name])




            new_constant: (name) ->
                if this.constants != {}
                    this.domNode.appendChild(domConstruct.create("br"))
                text = new TextBox({conn:name})
                label = domConstruct.create("label", 
                    {for:text.id, innerHTML: name+" : "})
                this.domNode.appendChild(label)
                this.addChild(text)
                #text.onChange = lang.hitch(this, "change_cons")
                this.constants[name] = text
                if this.cons.hasOwnProperty(name)
                    text.set("value", this.cons[name])




            get_connections: () ->
                connects = {}
                for c in Object.keys(this.connections)
                    connects[c] = this.connections[c].value
                return(connects)



            get_constants: () ->
                constants = {}
                for c in Object.keys(this.constants)
                    constants[c] = this.constants[c].value
                return(constants)