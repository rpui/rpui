///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

define \
    ["dojo/_base/declare", 
    "dojo/_base/lang",
    "dojo/dom-construct",
    "dojo/Evented", 
    "dijit/layout/ContentPane",
    "dojox/layout/TableContainer",
    "dijit/layout/LinkPane",
    "dijit/_WidgetBase",
    "app/server",
    "app/taskpane",
    "app/msg",
    "dojo/domReady!"],(
        declare, 
        lang,
        domConstruct,
        evented,
        ContentPane,
        TableContainer,
        LinkPane,
        WidgetBase,
        server,
        taskpane,
        msg) ->








        declare "TaskList", [ContentPane, evented],




            constructor: (args) ->
                declare.safeMixin(this, args)
                this.tList = []
                #this.nameDom = domConstruct.create("label", class:'bold', innerHTML:window.base.locales.get("node_title"))
                #this.hrDom = domConstruct.create("hr", style:'width:100%')
               



            postCreate:() ->
                #this.domNode.appendChild( this.nameDom)
                #this.domNode.appendChild(this.hrDom)
                if window.base.server_db["tasks"].length == 0
                    location.href = "/u/editnodetasks/"+window.base.server_db["node"]["name"]
                
                else if window.base.server_db["dock"].hasOwnProperty("nid")
                    docks = window.base.server_db["dock"]["order"]
                    nname = window.base.server_db["node"]["name"]
                    for k of docks
                        for t in window.base.server_db["tasks"]
                            if t["_id"] == docks[k]["tid"]
                                n = new TaskPane({
                                    "defs": docks[k],
                                    "task": t, 
                                    "nname":nname})
                                this.domNode.appendChild(n.domNode)
                                n.on("onSend", lang.hitch(this, "_onSend"))
                                this.tList.push(n)
                else
                    location.href = "/u/editdock/"+window.base.server_db["node"]["name"]



            _onSend:()->
                s = new stack()
                for n in this.tList
                    d = n.getData()
                    if d 
                        s.append(n.source, d)
                c = new cmdata("adminupdate", 
                    {"nid" : window.base.server_db["node"]["_id"],
                    "tasks":s.stack()})
                this.emit("onSend", c)




            update:(data)->
                for t in this.tList
                    for d in data
                        if d["data"]["tid"] == t.defs["tid"]
                            t.updateData(d)
