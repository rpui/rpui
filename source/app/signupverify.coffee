///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

require [
    "dojo/dom",
    "dojo/ready",
    "app/base.js",
    "dojo/domReady!" ], (
        dom,
        ready,
        base) ->



    ready(->

        base("signupverify")


        initialized = ->
            #l_topPane.destroyRecursive()
            window.base.handle_messages("notification", null,persistent = "true")
            if window.base.server_err
                dom.byId("lbutton").textContent = window.base.locales.get("signupbutton")
                dom.byId("lform").action = "/signup"
            else
                dom.byId("lbutton").textContent = window.base.locales.get("loginbutton")
                dom.byId("lform").action = "/login"


        window.base.load_locales(initialized)

        )
