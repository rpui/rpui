///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

define \
    ["dojo/_base/declare",
    "dojo/parser",
    "dojo/dom-construct",
    "dojo/json",
    "dojo/request", 
    "dojo/_base/config",
    "dojo/_base/window",
    "dojo/cookie",
    "dojo/store/Memory", 
    "dojo/_base/lang",
    "dojox/lang/functional/object",
    "dijit/registry",
    "dojo/dom",
    "app/i18n",
    "app/viewport",
    "app/nav",
    "app/umenu",
    "dojo/domReady!"],(
        declare,
        parser,
        construct,
        json,
        request, 
        config,
        win,
        cookie,
        memory,
        lang,
        func,
        registry,
        dom,
        i18n,
        viewport,
        nav) ->


        


        declare "base",[],




            constructor: (page_name) ->
                this.version = "OMM"    #One Man Maintained
                this.subversion = 1
                least_server_subver = 1

                console.log ("UI version : "+this.version+"."+this.subversion)

                l_mainContainer.resize()
                window.base = this
                this._page_name = page_name
                
                #Load viewport css
                this.viewport = viewport()
                
                #Load Page's style
                baselink = "<link rel='stylesheet' type='text/css' href='/app/css/"
                baselink = baselink.concat(this.viewport.mode()).concat("/base.css'>")
                construct.place(baselink, win.body())
                link = "<link rel='stylesheet' type='text/css' href='/app/css/"
                link = link.concat(this.viewport.mode()).concat("/")
                link = link.concat(page_name).concat(".css'>")
                construct.place(link, win.body())

                #Top Events
                dom.byId("w_nav_button").onclick = () -> 
                    w_mainStack.selectChild(w_mainStack_Nav)
                dom.byId("w_content_button").onclick = () -> 
                    w_mainStack.selectChild(w_mainStack_Content)
                dom.byId("w_user_button").onclick = () -> 
                    w_mainStack.selectChild(w_mainStack_User)
                         
                #Parse server messages
                try
                    data = json.parse(dom.byId("serverdata").text)
                    window.base.server_msg =  data.server_msg
                    window.base.server_err =  data.server_err
                    window.base.server_db =  json.parse(data.server_db)
                    console.log ("Server version : "+data.server_ver+"."+data.server_subver)
                catch
                    this._error ("Server data error")

                if data.server_ver != this.version
                    this._error("Incompatible server version")
                else if Number(data.server_subver) < this.least_server_subver
                    this._error("Incompatible server subversion")

                if document.cookie.search("user=") == -1 
                    delete window.base.server_db["uname"]

                if window.base.server_db.hasOwnProperty("uname")
                    window.base.source = {
                        "id" : "", 
                        "name" : "", 
                        "uname" : window.base.server_db["uname"],
                        "nname" : "admin"}
                else
                    window.base.source = {
                        "id" : "", 
                        "name" : "", 
                        "uname" : "",
                        "nname" : "admin"}




            dom_ready: () ->
                this.navlist = new NavList()
                this.umenu = new UserMenu()
                w_mainStack_Nav.addChild(this.navlist)
                w_mainStack_User.addChild(this.umenu)
                if window.base.source.uname != ""
                    this.umenu.add_item(window.base.locales.get("Logout"), 
                        lang.hitch(this, "logout"))




            load_locales: (callback) ->
                #Load locale files
                this._callback = callback
                this.locales = i18n(this._page_name,
                    lang.hitch(this, "_locales_ok"), 
                    this._error)




            _locales_ok: () ->
                this.dom_ready()
                l_mainContainer.resize()
                window.base._callback()




            _error: (msg)  ->
                alert("Initial error : ".concat(msg))




            # Show server messages
            # domid = dom id, pos = before/after, persistent = true/false
            handle_messages: (domid, pos ="before", persistent ="false") ->
                if window.base.server_err
                    m = window.base.server_err
                    c = "err"
                else if window.base.server_msg
                    m = window.base.server_msg
                    c = "msg"
                construct.place("<p id='pmessage'><label id='message' class='"+c+"'/></p>", 
                    domid,
                    pos)
                dom.byId("message")["textContent"] = window.base.locales.get(m)
                if persistent == "false"
                    setTimeout( () ->
                            construct.destroy("pmessage")
                        , 5000)




            logout: () ->
                document.cookie = "user=; expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/"
                document.cookie = "user-locale=; expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/"
                location.href = "/login"