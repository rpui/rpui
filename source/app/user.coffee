///
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
///

require [
    "dojo/ready",
    "dijit/registry",
    "dojo/_base/lang",
    "dojo/cookie",
    "app/base",
    "app/server",
    "app/msg",
    "app/nodelist",
    "dojo/domReady!" ], (
        ready,
        registry,
        lang,
        cookie,
        base,
        server,
        msg,
        nodelist) ->



    ready(->

        base("user")

        s = server(url="/xhr/alives", repeat=true)
        ids = []
        nodes = stack()
       
        for node in window.base.server_db["nodes"]
            ids.push(node["_id"])
        nodes.append(window.base.source, {"nodeids" : ids})




        initialized = ->
            
            w_nodelist = new nodelist(id:"w_nodelist")            
            w_mainStack_Content.addChild( w_nodelist)

            s.start()



            
        loaded = (response) ->
            registry.byId("w_nodelist").updateStatus(response[0].data.alive)




        error = (err) ->
            console.log (err)




        call = () ->
            s.run(nodes)




        s.on("loaded", loaded)
        s.on("error", error)
        s.on("ontime", call)

        try
            cookie("user-locale", window.base.server_db["user"]["lang"])
        catch e
            null
          
        window.base.load_locales(initialized)


        )
