#!/bin/bash
# Local workspace building script
: <<'COMMENT'
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
COMMENT









coffee -v
OUT=$?
if ! [ $OUT -eq 0 ];then
    echo "coffeescript not found"
    exit 1
fi

python3 -V
OUT=$?
if ! [ $OUT -eq 0 ];then
    echo "python3 not found"
    exit 1
fi


path=$(dirname $(readlink -f $0))
bundle=$path/bundle
ui=$path/../source
tw=$path/../../rptw/source
cd $path


if ! [ -f "nginx.conf" ]; then
    echo "nginx.conf not found"
    exit 1
fi
if ! [ -d "../source" ]; then
    echo "source files not found"
    exit 1
fi
if ! [ -d "../../rpui" ]; then
    echo "rpui path not found"
    exit 1
fi
if ! [ -d "../../rptw/source" ]; then
    echo "rptw path not found"
    exit 1
fi

if ! [ -f "dojo.tar.gz" ]; then
    echo -n "Dojo framework downloading..."
    curl http://download.dojotoolkit.org/release-1.14.0/dojo-release-1.14.0.tar.gz --output dojo.tar.gz
    if ! [ $OUT -eq 0 ];then
        exit 1
    else
        echo "ok"
    fi
fi

if ! [ -d "dojo" ]; then
    echo -n "Dojo framework extracting..."
    tar -xf dojo.tar.gz  1> /dev/null
    OUT=$?
    if ! [ $OUT -eq 0 ];then
        exit 1
    else
        echo "ok"
    fi
    mv dojo-release-1.14.0 dojo
fi




echo -n "Creating bundle..."
/bin/rm -rf $bundle > /dev/null
mkdir -p $bundle 1> /dev/null

/bin/cp -uRv $ui/* $bundle  1> /dev/null
if ! [ $OUT -eq 0 ];then
    exit 1
fi

/bin/cp -uRv $path/dojo $bundle/app  1> /dev/null
if ! [ $OUT -eq 0 ];then
    exit 1
else
    echo "ok"
fi

echo -n "UI Compiling..."
coffee -c $bundle/app/*.coffee
OUT=$?
if ! [ $OUT -eq 0 ];then
    exit 1
else
    echo "ok"   
fi

cd $bundle/app/lang
python3 regenerate.py


echo "Creating TW..."
for f in $tw/*; do
    if [ -d ${f} ]; then
        echo -n "$f..."
        /bin/cp -uRv $f/* $bundle/app/tw  1> /dev/null
        if ! [ $OUT -eq 0 ];then
            exit 1
        else
            echo "ok"
        fi
    fi
done

echo -n "TW Compiling..."
coffee -c $bundle/app/tw/*.coffee
OUT=$?
if ! [ $OUT -eq 0 ];then
   exit 1 
else
    echo "ok"
fi

cd $bundle/app/tw  1> /dev/null
python3 build.py 

echo -n "Cleaning..."
find . -name "*.coffee" -exec rm -f {} \;
echo "ok"
echo "Bundle is ready."