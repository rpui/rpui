#!/bin/bash
# Docker builder
: <<'COMMENT'
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
COMMENT







docker version 1>/dev/null
OUT=$?
if ! [ $OUT -eq 0 ];then
    echo "docker not found"
    exit 1
fi

path=$(dirname $(readlink -f $0))
cd $path
sudo docker build -t rpui .
OUT=$?
if [ $OUT -eq 0 ];then
    echo "Docker 'rpui' is ready."
else
    exit 1
fi
