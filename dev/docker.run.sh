#!/bin/bash
# Docker runner
: <<'COMMENT'
    This file is part of Rpui project.

    Rpui is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rpui is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rpui.  If not, see <http://www.gnu.org/licenses/>.
COMMENT








path=$(dirname $(readlink -f $0))
cd $path

docker stop rpui.test &>/dev/null
docker rm rpui.test &>/dev/null
docker run --name rpui.test -d -p 80:80 -v $(pwd)/bundle:/usr/share/nginx/html rpui
echo .
echo "Open http://localhost"
sleep 10
read -n1 -p "Press any key to stop container"
docker stop rpui.test 1>/dev/null
docker rm rpui.test 1>/dev/null